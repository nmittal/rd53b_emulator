-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-- Simple testbench for RD53B emulator on SLAC board
--
--

-------------------------------------------------------------------------------
library ieee;
use     ieee.std_logic_1164.all;
use     ieee.numeric_std.all;
use     std.textio.all;

use     work.rd53b_fpga_pkg.all;
use     work.rom_128x64bit_pkg.all;     -- Array of expected output data. Same data is loaded into hitmaker3 ROM

entity tb_rd53b_emulator is
end tb_rd53b_emulator;

architecture rtl of tb_rd53b_emulator is

-- Clock freq expressed in MHz
constant CLK_FREQ_250MHZ    : real      := 250.00;
constant CLK_FREQ_156MHZ    : real      := 156.25;

--constant CLK_FREQ_160MHZ    : real      := 160.00;
--constant CLK_FREQ_320MHZ    : real      := 320.00;

-- Clock periods
constant CLK_PER_250MHZ     : time      := integer(1.0E+6/(CLK_FREQ_250MHZ)) * 1 ps;
constant CLK_PER_156MHZ     : time      := integer(1.0E+6/(CLK_FREQ_156MHZ)) * 1 ps;

--constant CLK_PER_160MHZ     : time      := integer(1.0E+6/(CLK_FREQ_160MHZ)) * 1 ps;
--constant CLK_PER_320MHZ     : time      := integer(1.0E+6/(CLK_FREQ_320MHZ)) * 1 ps;

signal clk250               : std_logic := '0';     -- Clock from Yarr to SLAC
signal clk156               : std_logic := '0';
signal clk                  : std_logic := '0';		-- Same as clk156
signal sim_done             : boolean   := false;
signal reset                : std_logic := '1';

-- Procedure data driven to model of YARR ttc driver
signal tx_data              : std_logic_vector(15 downto 0);
signal tx_data_dv           : std_logic;

-- Outputs from YARR receiver model
signal yarr_rx_data0        : std_logic_vector(63 downto 0);    -- Received data
signal yarr_rx_data1        : std_logic_vector(63 downto 0);    -- Received data
signal yarr_rx_data2        : std_logic_vector(63 downto 0);    -- Received data
signal yarr_rx_data3        : std_logic_vector(63 downto 0);    -- Received data
signal yarr_rx_data_dv      : std_logic;                        -- Received data valid
signal yarr_rx_locked       : std_logic;                        -- Received data lock indicator
signal yarr_rx_service      : std_logic;                        -- Received sync indicator


-- FPGA port signals
signal USER_SMA_CLOCK_P     : std_logic;    -- Differential input clock
signal USER_SMA_CLOCK_N     : std_logic;
signal p_uart_rxd           : std_logic;
signal p_uart_txd           : std_logic;
signal ttc_ready            : std_logic;
signal ttc_data_p           : std_logic;
signal ttc_data_n           : std_logic;
signal cmd_out_p            : std_logic_vector( 3 downto 0);
signal cmd_out_n            : std_logic_vector( 3 downto 0);
signal led                  : std_logic_vector( 3 downto 0);
signal p_debug              : std_logic_vector(29 downto 0);

-- Signals to check recieved hit data against expected values from ROM package file
signal check_fail_0         : std_logic;
signal check_fail_1         : std_logic;
signal check_fail_2         : std_logic;
signal check_fail_3         : std_logic;
signal rx_hit_error         : std_logic := '0';
signal cnt_rx_data          : integer := 0;

-- Constants to make bit settings clearer
constant ON_CHIP_ID         : std_logic := '1';
constant ON_HIT_COMPRESS    : std_logic := '1';
constant ON_TOT_INCLUDE     : std_logic := '1';
constant ON_END_OF_STREAM   : std_logic := '1';

constant OFF_CHIP_ID        : std_logic := '0';
constant OFF_HIT_COMPRESS   : std_logic := '0';
constant OFF_TOT_INCLUDE    : std_logic := '0';
constant OFF_END_OF_STREAM  : std_logic := '0';


-------------------------------------------------------------
-- Delay
-------------------------------------------------------------
procedure clk_delay(
    constant nclks  : in  integer
) is
begin
    for I in 0 to nclks loop
        wait until clk'event and clk ='0';
    end loop;
end;

----------------------------------------------------------------
-- Print a string with no time or instance path.
----------------------------------------------------------------
procedure cpu_print_msg(
    constant msg    : in    string
) is
variable line_out   : line;
begin
    write(line_out, msg);
    writeline(output, line_out);
end procedure cpu_print_msg;

-------------------------------------------------------------
-- Procedure to check expected output from receiver
-------------------------------------------------------------
procedure check_rcv(
    signal   clk                        : in  std_logic;
    signal   service                    : in  std_logic;
    signal   data_rcv                   : in  std_logic_vector(63 downto 0);    -- Actual data received
    constant data_exp                   : in  std_logic_vector(63 downto 0);    -- Expected data
    signal   fail                       : out std_logic
) is

begin

    if (service = '0') then -- user data output
        if (data_rcv = data_exp) then
            cpu_print_msg("PASS");
            fail    <= '0';
        else
            cpu_print_msg("FAIL");
            fail    <= '1';
        end if;
    end if;

end;



-------------------------------------------------------------
-- Procedure to send a trigger comand
-------------------------------------------------------------
procedure cmd_trigger(
    signal   clk                        : in  std_logic;

    constant ntrig                      : in  integer;  -- Value 0 to 15
    constant ntag                       : in  integer;  -- Value 0 to 53

    signal   reg                        : out std_logic_vector(15 downto 0);
    signal   reg_dv                     : out std_logic
) is

begin

    wait until clk'event and clk='0';
    -- Build trigger command with tag. Tag is NOT encoded from 5-bit to 8-bits
    reg     <= C_ARR_CMD_TRIGGER(ntrig) & std_logic_vector(to_unsigned(ntag,8));
    reg_dv  <= '1';

    wait until clk'event and clk='0';
    reg     <= (others=>'0');
    reg_dv  <= '0';

end;


-------------------------------------------------------------
-- Procedure to send a register write command
-------------------------------------------------------------
procedure cmd_wr_reg(
    signal   clk                        : in  std_logic;

    constant chip_id                    : in  integer;
    constant addr                       : in  std_logic_vector( 8 downto 0);
    constant data                       : in  std_logic_vector(15 downto 0);

    signal   reg                        : out std_logic_vector(15 downto 0);
    signal   reg_dv                     : out std_logic
) is
variable v_data0 : std_logic_vector(4 downto 0);
begin

    -- First word is command with chip_id. ID is encoded from 5-bit to 8-bits.
    wait until clk'event and clk='0';
    reg     <= C_CMD_WRREG_0(15 downto 8) & C_ENC_5BIT_TO_8BIT(chip_id);
    reg_dv  <= '1';

    -- Second word is {0,addr[8:5]}, addr[4:0]. Each encoded from 5-bit to 8-bits.
    wait until clk'event and clk='0';
    reg     <= std_logic_vector(C_ENC_5BIT_TO_8BIT(to_integer(unsigned(addr(8 downto 5))))) & std_logic_vector(C_ENC_5BIT_TO_8BIT(to_integer(unsigned(addr(4 downto 0)))));
    reg_dv  <= '1';

    -- Third word is data[15:11], data[10:6]. Each encoded from 5-bit to 8-bits.
    wait until clk'event and clk='0';
    reg     <= std_logic_vector(C_ENC_5BIT_TO_8BIT(to_integer(unsigned(data(15 downto 11))))) & std_logic_vector(C_ENC_5BIT_TO_8BIT(to_integer(unsigned(data(10 downto 6)))));
    reg_dv  <= '1';

    -- Fourth word is data[5:1], {data[0],0000} . Each encoded from 5-bit to 8-bits.
    v_data0 := data(0) & "0000";
    wait until clk'event and clk='0';
    reg     <= std_logic_vector(C_ENC_5BIT_TO_8BIT(to_integer(unsigned(data(5 downto 1))))) & std_logic_vector(C_ENC_5BIT_TO_8BIT(to_integer(unsigned(v_data0))));
    reg_dv  <= '1';

    wait until clk'event and clk='0';
    reg     <= (others=>'0');
    reg_dv  <= '0';

end;

begin

    USER_SMA_CLOCK_P    <= clk250;
    USER_SMA_CLOCK_N    <= not(clk250);

    p_uart_rxd          <= '1';


    -------------------------------------------------------------
    -- YARR TTC driver
    -------------------------------------------------------------
    u_model_yarr_ttc : entity work.model_yarr_ttc
    port map
    (
        clk                 => clk156               , -- in  std_logic;
        reset               => reset                , -- in  std_logic;
        sim_done            => sim_done             , -- in  boolean;
        tx_data             => tx_data              , -- in  std_logic_vector(15 downto 0);    -- Commands
        tx_data_dv          => tx_data_dv           , -- in  std_logic;
        ready               => ttc_ready            , -- out std_logic;
        ttc_p               => ttc_data_p           , -- out std_logic;
        ttc_n               => ttc_data_n             -- out std_logic
    );


    -------------------------------------------------------------
    -- FPGA
    -------------------------------------------------------------
    u_on_chip_top : entity work.on_chip_top
    port map
    (
           USER_SMA_CLOCK_P => USER_SMA_CLOCK_P     , -- in
           USER_SMA_CLOCK_N => USER_SMA_CLOCK_N     , -- in

           p_uart_rxd       => p_uart_rxd           , -- in
           p_uart_txd       => p_uart_txd           , -- out

           ttc_data_p       => ttc_data_p           , -- in
           ttc_data_n       => ttc_data_n           , -- in

           cmd_out_p        => cmd_out_p            , -- out  [3:0]
           cmd_out_n        => cmd_out_n            , -- out  [3:0]

           led              => led                  , -- out  [3:0]
           p_debug          => p_debug                -- out [29:0]
    );


    -------------------------------------------------------------
    -- YARR data receiver. Descramble, deserializer
    -------------------------------------------------------------
    u_model_yarr_rcv : entity work.model_yarr_rcv
    port map
    (
        clk320              => '0'                  , -- in  std_logic; ** NOT USED **
        clk160              => clk156               , -- in  std_logic;
        reset               => reset                , -- in  std_logic;
        sim_done            => sim_done             , -- in  boolean;

        rx_serial_p         => cmd_out_p            , -- in  std_logic_vector( 3 downto 0);
        rx_serial_n         => cmd_out_n            , -- in  std_logic_vector( 3 downto 0);

        rx_data0            => yarr_rx_data0        , -- out std_logic_vector(63 downto 0);    -- Received data
        rx_data1            => yarr_rx_data1        , -- out std_logic_vector(63 downto 0);    -- Received data
        rx_data2            => yarr_rx_data2        , -- out std_logic_vector(63 downto 0);    -- Received data
        rx_data3            => yarr_rx_data3        , -- out std_logic_vector(63 downto 0);    -- Received data
        rx_data_dv          => yarr_rx_data_dv      , -- out std_logic;                        -- Received data valid
        rx_service          => yarr_rx_service      , -- out std_logic                         -- Received sync data word
        rx_locked           => yarr_rx_locked         -- out std_logic                         -- Received data lock indicator
    );


    -------------------------------------------------------------
    -- Generate clocks
    -------------------------------------------------------------
    pr_clk250 : process
    begin
        clk250  <= '0';
        wait for (CLK_PER_250MHZ/2);
        clk250  <= '1';
        wait for (CLK_PER_250MHZ-CLK_PER_250MHZ/2);
        if (sim_done=true) then
            wait;
        end if;
    end process;

    pr_clk156 : process
    begin
        clk156  <= '0';
        clk     <= '0';
        wait for (CLK_PER_156MHZ/2);
        clk156  <= '1';
        clk     <= '1';
        wait for (CLK_PER_156MHZ-CLK_PER_156MHZ/2);
        if (sim_done=true) then
            wait;
        end if;
    end process;


    -------------------------------------------------------------
    -- Main
    -------------------------------------------------------------
    pr_main : process
    begin

        -- Reset and drive starting values on all input signals
        reset                       <= '1';     -- Board testbench reset
        tx_data                     <= (others=>'0');
        tx_data_dv                  <= '0';

        clk_delay(10);
        reset                       <= '0';     -- Board testbench reset
        wait until yarr_rx_locked = '1';


        ------------------------------------------------------------------------
        -- Register write command
        ------------------------------------------------------------------------
        -- signal   clk                        : in  std_logic;
        --
        -- constant chip_id                    : in  integer;
        -- constant addr                       : in  std_logic_vector( 8 downto 0);
        -- constant data                       : in  std_logic_vector(15 downto 0);
        --
        -- signal   reg                        : out std_logic_vector(15 downto 0);
        -- signal   reg_dv                     : out std_logic
        ------------------------------------------------------------------------

        ------------------------------------------------------------------------
        -- Write 0x1234 to register address 1 on chip 3
        ------------------------------------------------------------------------
        cmd_wr_reg   (clk,  3, ADR_REG_001, X"1234",  tx_data, tx_data_dv);

        ------------------------------------------------------------------------
        -- Write 0x5678 to register address 2 on chip 3
        ------------------------------------------------------------------------
        cmd_wr_reg   (clk,  3, ADR_REG_002, X"5678",  tx_data, tx_data_dv);
        cmd_wr_reg   (clk,  3, ADR_REG_003, X"5000",  tx_data, tx_data_dv);
        wait for 2 us;

        ------------------------------------------------------------------------
        -- Trigger command
        -- Trigger 00TT with base tag 5 to chip 3
        ------------------------------------------------------------------------
        cmd_trigger(clk, 15 , 5, tx_data, tx_data_dv);

        wait for 60 us;

        sim_done    <= true;
        wait;

    end process;


    ------------------------------------------------------------------------
    -- Check output hit data against expected values from ROM package
    -- Four ROM values are used on every output. The ROM value address pointer
    -- wraps back to zero.
    ------------------------------------------------------------------------
    pr_check : process (reset, clk)
    begin
        if (reset = '1') then
            rx_hit_error  <= '1';
            cnt_rx_data   <= 0;

        elsif falling_edge(clk) then

            if (yarr_rx_data_dv = '1') then
                -- Channels are reversed in the connections between chip_output and aurora_tx_four_lane in RD53B emulator
                check_rcv( clk, yarr_rx_service, yarr_rx_data3, C_ARR_HITDATA_EXP(cnt_rx_data    ), check_fail_3);
                check_rcv( clk, yarr_rx_service, yarr_rx_data2, C_ARR_HITDATA_EXP(cnt_rx_data + 1), check_fail_2);
                check_rcv( clk, yarr_rx_service, yarr_rx_data1, C_ARR_HITDATA_EXP(cnt_rx_data + 2), check_fail_1);
                check_rcv( clk, yarr_rx_service, yarr_rx_data0, C_ARR_HITDATA_EXP(cnt_rx_data + 3), check_fail_0);

                if (cnt_rx_data < 124) then
                    cnt_rx_data     <= cnt_rx_data + 4;
                end if;

            end if;

            rx_hit_error    <=  check_fail_0 or check_fail_1 or check_fail_2 or check_fail_3;
        end if;

    end process;

end rtl;

