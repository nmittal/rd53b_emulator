vlib work

#RD53B Modules
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/RD53_top.sv
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/blink.vhd
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/ttc_top.v
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/SRL.v
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/shift_align.v
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/chip_output.sv
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/trigger_counter.v
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/command_process.sv
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/hitMaker3.sv
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/hit_generator3.sv
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/command_out.v
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/frame_buffer_four_lane.sv
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/aurora_tx_four_lane.sv
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/yarr_files/TX/aurora_tx_lane128.vhd
vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/scrambler_dummy.v
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/yarr_files/TX/gearbox66to32.vhd
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/yarr_files/TX/serdes32to8.vhd
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/yarr_files/TX/serdes8to1.vhd


#testing files
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/test/rd5b_fpga_pkg.vhd
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/test/rom_128x64bit_pkg.vhd
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/test/fifo_Nx16bit.vhd
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/test/model_yarr_ttc.vhd
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/test/model_yarr_rcv.vhd


#Xilinx IP cores
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/clk_wiz_SLAC/clk_wiz_SLAC_sim_netlist.vhdl
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/fifo_generator_0/fifo_generator_0_sim_netlist.vhdl
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/triggerFifo/triggerFifo_sim_netlist.vhdl
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/triggerTagFifo/TriggerTagFifo_sim_netlist.vhdl
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/hitDataFIFO/hitDataFIFO_sim_netlist.vhdl
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/rom_128x64bit/rom_128x64bit_sim_netlist.vhdl
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/fifo_generator_1/fifo_generator_1_sim_netlist.vhdl
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/fifo_generator_2/fifo_generator_2_sim_netlist.vhdl
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/fifo_one_to_four/fifo_one_to_four_sim_netlist.vhdl


#XILINX ILA IPs
#vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_command_received/ila_command_received_sim_netlist.vhdl
#vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_output_data_top_level/ila_output_data_top_level_sim_netlist.vhdl
#vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_ttc/ila_ttc_sim_netlist.vhdl
#vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_chip_out/ila_chip_out_sim_netlist.vhdl
#vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_command_process/ila_command_process_sim_netlist.vhdl
#vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/ip/ila_hitgen_fifo/ila_hitgen_fifo_sim_netlist.vhdl



vlog -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/on_chip/on_chip_top_SLAC_sim.sv
vcom -work work ../Vivado/RD53B_SLAC/RD53B_Emu.srcs/sources_1/imports/test/tb_rd53b_emulator.vhd

vsim -t 1fs -novopt tb_rd53b_emulator -L unisim -L secureip -L unifast -L unimacro

view signals
view wave

#do wave_RD53B.do

run 24us