--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
--Date        : Mon Nov 11 17:19:18 2019
--Host        : RATBAG running 64-bit major release  (build 9200)
--Command     : generate_target ps1.bd
--Design      : ps1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ps1 is
  port (
    clk : in STD_LOGIC;
    dip_switches_4bits_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    gpio1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio2_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    gpio2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    pit1_toggle : out STD_LOGIC;
    push_buttons_5bits_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 );
    reset_n : in STD_LOGIC;
    uart_rxd : in STD_LOGIC;
    uart_txd : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of ps1 : entity is "ps1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=ps1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=1,numReposBlks=1,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_board_cnt=7,synth_mode=OOC_per_IP}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of ps1 : entity is "ps1.hwdef";
end ps1;

architecture STRUCTURE of ps1 is
  component ps1_microblaze_mcs_0_0 is
  port (
    Clk : in STD_LOGIC;
    Reset : in STD_LOGIC;
    PIT1_Toggle : out STD_LOGIC;
    UART_rxd : in STD_LOGIC;
    UART_txd : out STD_LOGIC;
    GPIO1_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO1_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO2_tri_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO2_tri_o : out STD_LOGIC_VECTOR ( 31 downto 0 );
    GPIO3_tri_i : in STD_LOGIC_VECTOR ( 3 downto 0 );
    GPIO4_tri_i : in STD_LOGIC_VECTOR ( 4 downto 0 )
  );
  end component ps1_microblaze_mcs_0_0;
  signal Clk_0_1 : STD_LOGIC;
  signal mcs0_GPIO3_TRI_I : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal mcs0_GPIO4_TRI_I : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal mcs0_PIT1_Toggle : STD_LOGIC;
  signal microblaze_mcs_0_GPIO1_TRI_I : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal microblaze_mcs_0_GPIO1_TRI_O : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal microblaze_mcs_0_GPIO2_TRI_I : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal microblaze_mcs_0_GPIO2_TRI_O : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal microblaze_mcs_0_UART_RxD : STD_LOGIC;
  signal microblaze_mcs_0_UART_TxD : STD_LOGIC;
  signal reset_1 : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 CLK.CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME CLK.CLK, CLK_DOMAIN ps1_Clk_0, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of reset_n : signal is "xilinx.com:signal:reset:1.0 RST.RESET_N RST";
  attribute X_INTERFACE_PARAMETER of reset_n : signal is "XIL_INTERFACENAME RST.RESET_N, POLARITY ACTIVE_HIGH";
  attribute X_INTERFACE_INFO of uart_rxd : signal is "xilinx.com:interface:uart:1.0 uart RxD";
  attribute X_INTERFACE_INFO of uart_txd : signal is "xilinx.com:interface:uart:1.0 uart TxD";
  attribute X_INTERFACE_INFO of dip_switches_4bits_tri_i : signal is "xilinx.com:interface:gpio:1.0 dip_switches_4bits ";
  attribute X_INTERFACE_INFO of gpio1_tri_i : signal is "xilinx.com:interface:gpio:1.0 gpio1 TRI_I";
  attribute X_INTERFACE_INFO of gpio1_tri_o : signal is "xilinx.com:interface:gpio:1.0 gpio1 TRI_O";
  attribute X_INTERFACE_INFO of gpio2_tri_i : signal is "xilinx.com:interface:gpio:1.0 gpio2 TRI_I";
  attribute X_INTERFACE_INFO of gpio2_tri_o : signal is "xilinx.com:interface:gpio:1.0 gpio2 TRI_O";
  attribute X_INTERFACE_INFO of push_buttons_5bits_tri_i : signal is "xilinx.com:interface:gpio:1.0 push_buttons_5bits ";
begin
  Clk_0_1 <= clk;
  gpio1_tri_o(31 downto 0) <= microblaze_mcs_0_GPIO1_TRI_O(31 downto 0);
  gpio2_tri_o(31 downto 0) <= microblaze_mcs_0_GPIO2_TRI_O(31 downto 0);
  mcs0_GPIO3_TRI_I(3 downto 0) <= dip_switches_4bits_tri_i(3 downto 0);
  mcs0_GPIO4_TRI_I(4 downto 0) <= push_buttons_5bits_tri_i(4 downto 0);
  microblaze_mcs_0_GPIO1_TRI_I(31 downto 0) <= gpio1_tri_i(31 downto 0);
  microblaze_mcs_0_GPIO2_TRI_I(31 downto 0) <= gpio2_tri_i(31 downto 0);
  microblaze_mcs_0_UART_RxD <= uart_rxd;
  pit1_toggle <= mcs0_PIT1_Toggle;
  reset_1 <= reset_n;
  uart_txd <= microblaze_mcs_0_UART_TxD;
mcs0: component ps1_microblaze_mcs_0_0
     port map (
      Clk => Clk_0_1,
      GPIO1_tri_i(31 downto 0) => microblaze_mcs_0_GPIO1_TRI_I(31 downto 0),
      GPIO1_tri_o(31 downto 0) => microblaze_mcs_0_GPIO1_TRI_O(31 downto 0),
      GPIO2_tri_i(31 downto 0) => microblaze_mcs_0_GPIO2_TRI_I(31 downto 0),
      GPIO2_tri_o(31 downto 0) => microblaze_mcs_0_GPIO2_TRI_O(31 downto 0),
      GPIO3_tri_i(3 downto 0) => mcs0_GPIO3_TRI_I(3 downto 0),
      GPIO4_tri_i(4 downto 0) => mcs0_GPIO4_TRI_I(4 downto 0),
      PIT1_Toggle => mcs0_PIT1_Toggle,
      Reset => reset_1,
      UART_rxd => microblaze_mcs_0_UART_RxD,
      UART_txd => microblaze_mcs_0_UART_TxD
    );
end STRUCTURE;
