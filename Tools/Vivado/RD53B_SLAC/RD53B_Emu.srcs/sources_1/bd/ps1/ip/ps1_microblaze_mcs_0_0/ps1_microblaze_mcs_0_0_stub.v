// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Mon Nov 11 17:23:42 2019
// Host        : RATBAG running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top ps1_microblaze_mcs_0_0 -prefix
//               ps1_microblaze_mcs_0_0_ ps1_microblaze_mcs_0_0_stub.v
// Design      : ps1_microblaze_mcs_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7k160tfbg484-2
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "bd_bec1,Vivado 2017.4" *)
module ps1_microblaze_mcs_0_0(Clk, Reset, PIT1_Toggle, UART_rxd, UART_txd, 
  GPIO1_tri_i, GPIO1_tri_o, GPIO2_tri_i, GPIO2_tri_o, GPIO3_tri_i, GPIO4_tri_i)
/* synthesis syn_black_box black_box_pad_pin="Clk,Reset,PIT1_Toggle,UART_rxd,UART_txd,GPIO1_tri_i[31:0],GPIO1_tri_o[31:0],GPIO2_tri_i[31:0],GPIO2_tri_o[31:0],GPIO3_tri_i[3:0],GPIO4_tri_i[4:0]" */;
  input Clk;
  input Reset;
  output PIT1_Toggle;
  input UART_rxd;
  output UART_txd;
  input [31:0]GPIO1_tri_i;
  output [31:0]GPIO1_tri_o;
  input [31:0]GPIO2_tri_i;
  output [31:0]GPIO2_tri_o;
  input [3:0]GPIO3_tri_i;
  input [4:0]GPIO4_tri_i;
endmodule
