//----------------------------------------------------------------------------
// Generate hit data.
// Data is pre-loaded into a RAM and read out when 'trigger_i' goes high.
//----------------------------------------------------------------------------
// tclk: "clk80",
// hclk: "clk40",
// dclk: "clk160";
// using hclk, fifo reads at dclk
//----------------------------------------------------------------------------
`timescale 1ps/1ps
module hit_generator3
(
    input   logic           rst_i           , // Active high reset
    input   logic           clk_i           , // Clock

    input                   uart_rxd        , // Debug (unused)
    output                  uart_txd        , // Debug (unused)

    input   logic   [15:0]  config_reg_i    , // Unused. Used in RD53A
    input   logic           trigger_i       , // Start data output
    input   logic   [31:0]  trigger_info_i  , // Unused. In RD53A was passed through to the first 32-bit chunk sent out before the first hit data chunk
    output  logic   [63:0]  trigger_data_o  , // Output data
    output  logic           done_o            // Low while output of data is occurring
);
//----------------------------------------------------------------------------


    reg     [ 6:0]  rom_addr;  
    reg     [63:0]  rom_dout;
    reg             busy;
    localparam      SIZE_STREAM = 20;

    assign  uart_txd    = 1'b1; // Unused output port


    //------------------------------------------------------------------------
    // ROM address counter. Started by trigger. Sets busy high and counts up
    // to SIZE_STREAM-1
    //------------------------------------------------------------------------
    always_ff @(posedge clk_i)
    begin

        if (rst_i) begin
            busy        <= 1'b0;
            rom_addr    <= 0;

        end else if (!busy & trigger_i) begin
            busy        <= 1'b1;
            rom_addr    <= rom_addr + 1;

        end else if (busy & (rom_addr < SIZE_STREAM)) begin
            busy        <= 1'b1;
            rom_addr    <= rom_addr + 1;

        end else begin
            busy        <= 1'b0;
            rom_addr    <= 0;
        end

    end

    assign done_o           = ~busy;
    assign trigger_data_o   = rom_dout;


    //------------------------------------------------------------------------
    // ROM pre-loaded with output stream data
    //------------------------------------------------------------------------
    rom_128x64bit u_rom_128x64bit  //NM => changed the module from 128 to 256
    (
        .clka   ( clk_i     )   ,   // in  std_logic;
        .addra  ( rom_addr  )   ,   // in  std_logic_vector( 6 downto 0);
        .douta  ( rom_dout  )       // out std_logic_vector(63 downto 0)
    );


endmodule
