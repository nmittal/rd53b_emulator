//----------------------------------------------------------------------
// RD53B Top Level FPGA for SLAC Readout board with 156.25 MHz clock
// on SMA inputs from YARR board
//----------------------------------------------------------------------

// This module is the top level module for the on-chip electronics.
// That includes the RD53 emulators and the required PLLs.
// PLLs were separated from the emulators so that we could maximize
// the number of emulators stampable in the FPGA, rather than
// be limited by the number of PLLs.

// The clocks and TTC must be forwarded from the DAQ, but they are
// shared by all the emulators, so only one of each must be present
// for the system to work.
// Each emulator outputs 4 differential lanes of data.


module on_chip_top
(
    input           USER_SMA_CLOCK_P                ,
    input           USER_SMA_CLOCK_N                ,

    input           p_uart_rxd                      ,
    output          p_uart_txd                      ,

    input           ttc_data_p                      ,
    input           ttc_data_n                      ,
    output  [3:0]   cmd_out_p                       ,
    output  [3:0]   cmd_out_n                       ,

    output  [3:0]   led                             ,
    output [29:0]   p_debug
);

logic rst;
logic trig_out;

assign rst = 1'b0;

// Global clocks
logic clk20, clk40, clk80, clk160;
logic ttc_data;                         // Single-ended TTC signal
logic uart_rxd   ;                      // Debug
logic uart_txd   ;                      // Debug
logic mmcm_locked;                      // MMCM locked signal
logic rst_lock_halt;                    // High during reset or when MMCM not locked

logic   [3:0]   chip_id;                //
assign  chip_id = 4'b0011;

// wait for the mmcm to lock before releasing submodules from reset
assign rst_lock_halt = rst | !mmcm_locked;


    //----------------------------------------------------
    // Internal clocks generated from incoming clk sent over SMA or VHDCI
    //----------------------------------------------------
    clk_wiz_SLAC u_clk_wiz_SLAC
    (
        .reset      (rst        ),

        .clk_in1_p  (USER_SMA_CLOCK_P   ),
        .clk_in1_n  (USER_SMA_CLOCK_N   ),

        .clk_out1   (           ),  // 160 MHz  Not used
        .clk_out2   (clk160     ),  // 160 MHz
        .clk_out3   (clk80      ),  //  80 MHz
        .clk_out4   (clk40      ),  //  40 MHz
        .clk_out5   (           ),  //  40 MHz  Not used
        .clk_out6   (clk20      ),  //  20 MHz
        .clk_out7   (           ),  // 320 MHz

        .locked     (mmcm_locked)
    );


    //----------------------------------------------------
    // Turn differential TTC signal into single-ended
    //----------------------------------------------------
    IBUFDS          u_IBUFDS_ttc(
        .I  (ttc_data_p  ),
        .IB (ttc_data_n  ),
        .O          (ttc_data    )
    );


    //----------------------------------------------------
    // One emulator instance
    //----------------------------------------------------
    RD53_top        u_RD53_top (
        .rst            (rst_lock_halt  ),
        .clk20      (clk20          ),
        .clk40          (clk40          ),
        .clk80          (clk80          ),
        .clk160         (clk160         ),

        .uart_rxd       (uart_rxd       ),
        .uart_txd       (uart_txd       ),

        .ttc_data   (ttc_data       ),
        .button1    (1'b0           ),
        .chip_id    (chip_id        ),

        .cmd_out_p      (cmd_out_p      ),
        .cmd_out_n      (cmd_out_n      ),

        .trig_out       (trig_out       ),
        .ilaActivate    (1'b0           ),

		.p_debug        (               )   //LEDs debug port -> wrreg,rdreg,trigger_r 

    );

    // Connections to FPGA UART pins to RD53 UART ports
    assign  p_uart_txd      = uart_rxd;
    assign  uart_rxd        = p_uart_rxd;


    logic           flash      ;
    logic           tick_sec   ;
    logic           tick_msec  ;
    logic           tick_usec  ;
    logic   [1:0]   pulse      ;
    logic   [1:0]   stretched  ;


    //----------------------------------------------------
    // Misc logic for timing pulses and LED signals
    //----------------------------------------------------
    blink u_blink
    (
        .reset          (rst        ),  // in   std_logic;
        .clk            (clk160     ),  // in   std_logic;
        .flash          (flash      ),  // out  std_logic;
        .tick_sec       (tick_sec   ),  // out  std_logic;    // Output tick every 1 sec
        .tick_msec      (tick_msec  ),  // out  std_logic;    // Output tick every 1 msec
        .tick_usec      (tick_usec  ),  // out  std_logic;    // Output tick every 1 usec
        .pulse          (pulse      ),  // in   std_logic_vector(1 downto 0);
        .stretched      (stretched  )   // out  std_logic_vector(1 downto 0)
    );

    assign pulse[0] = ttc_data;
    assign pulse[1] = trig_out;

    // Drive LEDs labelled DS1-4 on SLAC board
    assign led[0]   = flash;                // DS1
    assign led[1]   = mmcm_locked;          // DS2
    assign led[2]   = stretched[0];         // DS3
    assign led[3]   = stretched[1];         // DS4

    assign p_debug[0]       = tick_msec;
    assign p_debug[1]       = mmcm_locked;
    assign p_debug[2]       = ttc_data;
    assign p_debug[3]       = trig_out;
    assign p_debug[29:4]    = 26'b0;


endmodule