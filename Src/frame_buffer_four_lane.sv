// Engineer: Michael Walsh
// Date: 5/29/2018
// Module: Frame Buffer Four Lane
// Description: assembles individual frames
// into bundles of 4 frames to be sent to TX. Takes input
// directly from command_out. Additional logic
// in place to guarentee strict alignment 
// Testing: fb.do (runs randomly infinitely)

`timescale 1ps/1ps
module frame_buffer_four_lane
(
    input         rst,
    input         clk80,
    input         clk160,

    input  [63:0] frame,
    input         service_frame,
    input         frame_valid,
    input         present_frame,

    output [63:0] frame_hold [0:3],
    output [ 0:3] service_hold,

    input         trig_out,
    output        ready_d,
    output        ready_s
);


localparam      IDLE = 64'h1E00000000000000;  
logic   [7:0]   valid_data_n; 
logic   [7:0]   valid_ser_n; 
logic           ready_n; 


// output from s_pout units
logic   [0:3][64:0] buffer_data_r; 
logic   [0:3][64:0] buffer_ser_r; 


// Output from s_to_p unit with packed/fixed conversion 
logic   [64:0]      frame_hold_pre_n [0:3];

// "yumi": show me new data 
// each for respective s_pout unit 
logic next_data_yumi_r;
logic next_ser_yumi_r;

 
// present_frame delayed one clock cycle 
logic  present_frame_delay_r;
// pulse of present_frame that shares the same posedge 
logic  present_frame_pulse_n; 

always_ff @(posedge clk80 or posedge rst) 
begin
    if (rst)
        present_frame_delay_r <= 0; 
    else 
        present_frame_delay_r <= present_frame; 
end  

assign present_frame_pulse_n = (!present_frame_delay_r && present_frame);

logic empty_data;
logic fifo_data_rd;
logic fifo_data_wr;
logic [7:0] data_count;

assign fifo_data_rd = next_data_yumi_r && present_frame_pulse_n;
assign fifo_data_wr = frame_valid && !service_frame;

//--------------------------------------------
// FIFO for Data Frame data
//--------------------------------------------
fifo_one_to_four    u_fifo_data_s_pout 
(
    .rst            (rst                    ),

    .wr_clk         (clk160                 ),
    .din            ({service_frame,frame}  ),
    .wr_en          (fifo_data_wr           ),

    .rd_clk         (clk80                  ),
    .rd_en          (fifo_data_rd           ),
    .dout           (buffer_data_r          ),

    .full           (ready_d                ),
    .empty          (empty_data             ),
    .rd_data_count  (data_count             ),
    .wr_rst_busy    (                       ),
    .rd_rst_busy    (                       )
);


assign valid_data_n[3] = !empty_data;
assign valid_data_n[2] = !empty_data;
assign valid_data_n[1] = !empty_data;
assign valid_data_n[0] = !empty_data;
assign valid_data_n[7] = 1'b0;
assign valid_data_n[6] = 1'b0;
assign valid_data_n[5] = 1'b0;
assign valid_data_n[4] = 1'b0;


logic       empty_ser;
logic [7:0] ser_count;
logic       fifo_ser_wr;
logic       fifo_ser_rd;

assign  fifo_ser_wr     = frame_valid && service_frame;
assign  fifo_ser_rd     = next_ser_yumi_r && present_frame_pulse_n;

//--------------------------------------------
// FIFO for Service Frame data
//--------------------------------------------
fifo_one_to_four u_fifo_ser_s_pout 
(
    .rst            (rst                    ),
    .wr_clk         (clk160                 ),
    .rd_clk         (clk80                  ),
    .din            ({service_frame,frame}  ),
    .wr_en          (fifo_ser_wr            ),
    .rd_en          (fifo_ser_rd            ),
    .dout           (buffer_ser_r           ),
    .full           (ready_s                ),
    .empty          (empty_ser              ),
    .rd_data_count  (ser_count              ),
    .wr_rst_busy    (                       ),
    .rd_rst_busy    (                       )
);
 
assign valid_ser_n[3] = !empty_ser;
assign valid_ser_n[2] = !empty_ser;
assign valid_ser_n[1] = !empty_ser;
assign valid_ser_n[0] = !empty_ser;
assign valid_ser_n[7] = 1'b0;
assign valid_ser_n[6] = 1'b0;
assign valid_ser_n[5] = 1'b0;
assign valid_ser_n[4] = 1'b0;

//--------------------------------------------
// Output State Machine 
//--------------------------------------------
enum{idle_s = 0,  data_s = 1,  ser_s} state, next_state; 
int i; 

always_ff @(posedge clk80 or posedge rst) begin //DGS
    if(rst) begin 
        state <= idle_s; 
    end else if (present_frame_pulse_n) begin 
        state <= next_state; 
    end else begin 
        state <= state;
    end  
end 

always_comb begin

   case (state)

       // no new frames to send out,send out idle bundle 
       idle_s: begin 
            if((valid_ser_n[3] & valid_ser_n[2] & valid_ser_n[1] & valid_ser_n[0]))
                next_state = ser_s;
            else if((valid_data_n[3] & valid_data_n[2] & valid_data_n[1] & valid_data_n[0])) 
                next_state = data_s; 
            else 
                next_state = idle_s; 
       end

       // hitdata is available to send out 
       data_s: begin
            if((/*&valid_ser_n[3:0]*/valid_ser_n[3] & valid_ser_n[2] & valid_ser_n[1] & valid_ser_n[0]))
                next_state = ser_s;
            // only stay on data if 
            // another full frame is avalible 
            else if((data_count >= 2)) //NM => changed from 2  
                next_state = data_s; 
            else 
                next_state = idle_s; 
       end

       // service/register frame data to send out 
       ser_s: begin 
            // only stay on service if
            // another full frame is avalible
            if((ser_count >= 2))//NM => changed from 2 
                next_state = ser_s; 
            else if((/*&valid_data_n[3:0]*/valid_data_n[3] & valid_data_n[2] & valid_data_n[1] & valid_data_n[0])) 
                next_state = data_s; 
            else 
                next_state = idle_s; 
       end 
    endcase
end 


//--------------------------------------------
// State based logic 
// muxing between outputs from either
// service/idle/data    
//--------------------------------------------
always_comb begin 
    int i; 

    for(i = 0; i < 4; i = i + 1) 
    begin 
        if(state == idle_s)
            frame_hold_pre_n[i] = {1'b1, IDLE};
        else if (state == data_s) 
            frame_hold_pre_n[i] = buffer_data_r[i]; 
        else
            frame_hold_pre_n[i] = buffer_ser_r[i];
    end

    // Need to clear last read bundle of data from s_pout modules 
    if (state == data_s) begin 
        next_data_yumi_r    <= 1'b1; 
        next_ser_yumi_r     <= 1'b0; 
    end else if (state == ser_s) begin 
        next_ser_yumi_r     <= 1'b1; 
        next_data_yumi_r    <= 1'b0; 
    end else begin 
        next_data_yumi_r    <= 1'b0;
        next_ser_yumi_r     <= 1'b0; 
    end     

end 


//--------------------------------------------
// Packed-unpacked conversion to outputs
//--------------------------------------------
genvar j; 
for (j = 0; j < 4; j = j+1) 
begin 
    assign frame_hold[j]    = frame_hold_pre_n[j][63:0];
    assign service_hold[j]  = frame_hold_pre_n[j][64]; 
end 

/*
ilaFourBuff     u_ila_FourBuff (
    .clk(clk160)                    ,
    .probe0(frame)                  ,
    .probe1(frame_valid)            ,
    .probe2(present_frame)          ,
    .probe3(buffer_data_r[0])       ,
    .probe4(buffer_data_r[1])       ,
    .probe5(buffer_data_r[2])       ,
    .probe6(buffer_data_r[3])       ,
    .probe7(buffer_ser_r[0])        ,
    .probe8(buffer_ser_r[1])        ,
    .probe9(buffer_ser_r[2])        ,
    .probe10(buffer_ser_r[3])       ,
    .probe11(trig_out)              ,
    .probe12(service_frame)         ,
    .probe13(valid_ser_n)           ,
    .probe14(valid_data_n)          ,
    .probe15(state)                 ,
    .probe16(frame_hold_pre_n[0])   ,
    .probe17(frame_hold_pre_n[1])   ,
    .probe18(frame_hold_pre_n[2])   ,
    .probe19(frame_hold_pre_n[3])   ,
    .probe20(ready_s)               ,
    .probe21(ready_d)
);
*/

endmodule
