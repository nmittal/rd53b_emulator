//---------------------------------------------------------------------
// Command formatting and hitData formatted
// passed to frame_buffer_four_lane
// 
// Autoread data is fixed incrementing.
// TODO correct autodata output
//---------------------------------------------------------------------
`timescale 1ps/1ps

module command_out(
   input            rst,
   input            clk160,
   input            clk80,

   input            hitData_empty,
   output           next_hit,
   input    [63:0]  hitin,

   input            wr_cmd,
   input    [15:0]  cmdin,
   output           cmd_full,

   input            wr_adx,
   input    [ 8:0]  adxin,
   output           adx_full,

   output   [63:0]  data_out,
   output           data_out_valid,
   output           service_frame,

   input [7:0][25:0] auto_read_i 
);

// The nth frame when a service is frame is due
localparam DATA_FRAMES = 193;//49;

// IDLE block per aurora spec
localparam IDLE     = 64'h1E00_0000_0000_0000;

// The nth bundle (4 frames) that it is suggested
// a channel bonding bundle get sent out
localparam CB_WAIT = 42;

localparam sync_pattern = 16'b1000_0001_0111_1110;  // 0x817E

reg [ 8:0]  service_counter; 
reg [ 9:0]  adx_wait;
reg [15:0]  cmd_wait;
reg [ 1:0]  data_available;
reg [63:0]  data_out_prereverse;
reg [ 3:0]  startup_wait_cnt;
reg [11:0]  cb_wait_cnt;
reg         data_out_valid_r;

// If data is unavalible, send idle frame
wire [63:0] hitData_muxed;
assign      hitData_muxed = (hitData_empty) ? IDLE : hitin;
//assign    hitData_muxed = (next_hit) ? hitin : IDLE; // Old version

wire [15:0] cmd_out;
wire [8:0] adx_out;
wire        cmd_valid;
wire        adx_valid;
wire        rd_cmd;
wire        rd_adx;


//--------------------------------------------
// Command FIFO
// Input width 16, output width 16
// Depth 32
//--------------------------------------------
fifo_generator_1 u_fifo_cmd(
   .rst             (rst                ),

   .wr_clk          (clk80              ),
   .din             (cmdin              ),
   .wr_en           (wr_cmd             ),

   .rd_clk          (clk160             ),
   .rd_en           (rd_cmd             ),
   .dout            (cmd_out            ),

   .full            (cmd_full           ),
   .empty           (cmd_valid          ),
   .wr_rst_busy     (                   ),
   .rd_rst_busy     (                   )
);

//--------------------------------------------
// Register read FIFO?
// Input width 9, output width 9
// Depth 32
//--------------------------------------------
fifo_generator_2 u_fifo_adx(
    .rst            (rst),

    .wr_clk         (clk80),
    .din            (adxin),
    .wr_en          (wr_adx),

    .rd_clk         (clk160),
    .rd_en          (rd_adx),
    .dout           (adx_out),

    .full           (adx_full),
    .empty          (adx_valid),
    .wr_rst_busy    (         ),
    .rd_rst_busy    (         )
);


assign data_out_valid = data_out_valid_r;

//--------------------------------------------
// Boot condition state machine
// Checks coming out of reset if
// the position of clk160 relative to clk80
//--------------------------------------------
reg high;
always @(negedge clk160)
    high <= clk80;


// Bootup condition flag
reg sync_flag;

//--------------------------------------------
//Logic that pulls data from hitmaker fifo
//--------------------------------------------
assign next_hit =
	// Every other cycle of service_counter
	(service_counter[0] &&
	// sending service frames are not being sent out
	!((service_counter >= DATA_FRAMES * 2 - 3) && (service_counter <= DATA_FRAMES * 2 + 3)) &&
    // hitdata is available
	!(hitData_empty)) &&
	// Don't pull when channel bonding frames are being sent out
	!(cb_wait_cnt == CB_WAIT && (service_counter >= DATA_FRAMES * 2 + 5) && (service_counter <= DATA_FRAMES * 2 + 11));


//--------------------------------------------------------
// A pulse alongside data_out_valid while a service frame 
// or channel bonding frame is being sent out
//--------------------------------------------------------
assign service_frame =
    // If service frames are being sent during Nth cycle or
    ((((service_counter >= DATA_FRAMES * 2 - 2) && (service_counter <= DATA_FRAMES * 2 + 4)) ||
    // If channel bonding frames are being sent or
    ((cb_wait_cnt == CB_WAIT) && (service_counter >= DATA_FRAMES * 2 + 5))) &&
    // when data_out is valid
    data_out_valid);


assign rd_cmd = ~cmd_valid &&
    ((service_counter == DATA_FRAMES * 2 - 6) ||
     (service_counter == DATA_FRAMES * 2 - 4));

assign rd_adx = ~adx_valid &&
    ((service_counter == DATA_FRAMES * 2 - 6) ||
     (service_counter == DATA_FRAMES * 2 - 4));

reg adx_valid_r, cmd_valid_r;


//--------------------------------------------
//--------------------------------------------
always @ (posedge clk160) begin
    if (rst) begin
        adx_valid_r <= 0;
        cmd_valid_r <= 0;
    end else begin
        adx_valid_r <= ~adx_valid;
        cmd_valid_r <= ~cmd_valid;
    end
end


//------------------------------------------------
//------------------------------------------------
always @ (posedge clk160 or posedge rst) begin

    if (rst) begin
        startup_wait_cnt    <= 3'b0;
        service_counter     <= 9'b111111111;
        data_out_prereverse <= 64'b0;
      //data_out_valid_r    <= 1'b0;
        adx_wait            <= 9'b0;
        cmd_wait            <= 16'b0;
        cb_wait_cnt         <= 12'b0;
        sync_flag           <= 1'b0;
    end 
    else if (sync_flag == 1'b0) begin

        if (high)
            service_counter <= service_counter + 1;
        else
            service_counter <= service_counter;

        sync_flag <= 1'b1;

    end 
    else begin

        // Update service counter every cycle
        service_counter <= service_counter + 1;

        // Get first data chunk
        if (service_counter == DATA_FRAMES * 2 - 6) begin

            if (cmd_valid_r && adx_valid_r) begin
                data_available <= 2'b01;
            end
            else if (!cmd_valid_r && !adx_valid_r) begin
                data_available <= 2'b00;
            end
            else begin
                $display("Invalid FIFO configuration at %d", $time);
            end

        end

        // Save first data chunk
        else if (service_counter == DATA_FRAMES * 2 - 5) begin
            if (data_available) begin
                adx_wait <= {1'b0, adx_out};
                cmd_wait <= cmd_out;
            end
            data_out_prereverse <= hitData_muxed;
        end

        // Get second data chunk
        else if (service_counter == DATA_FRAMES * 2 - 4) begin
            if (cmd_valid_r && adx_valid_r) begin
                data_available <= 2'b10;
            end
            else if (!cmd_valid_r && !adx_valid_r) begin
                data_available <= data_available;
            end
            else begin
                $display("Invalid FIFO configuration at %d", $time);
            end
        end

        // Concatenate the data chunks and apply to output lines
        else if (service_counter == DATA_FRAMES * 2 - 3) begin
            if (data_available == 2'b10) begin          // Both reg from read commands
                data_out_prereverse <= {8'hD2, 4'b0000, adx_wait, cmd_wait, 1'b0, adx_out, cmd_out};
            end
            else if (data_available == 2'b01) begin     // First reg from read cmd, Second from autoread
                data_out_prereverse <= {8'h99, 4'b0000, adx_wait, cmd_wait, auto_read_i[0]};
            end
            else begin  // If user_read data is not available, send 2 auto-read registers
                data_out_prereverse[63:52] <= 12'hB40;
                data_out_prereverse[51:26] <= auto_read_i[0];       // 10-bit address, 16-bit data
                data_out_prereverse[25:0]  <= auto_read_i[1] ;      // 10-bit address, 16-bit data
            end     // TODO What about 0x55 codes
            data_available <= 2'b0;
        end

        // Reiterate register/service frame
        else if ((service_counter == DATA_FRAMES * 2 - 2)) begin
                    data_out_prereverse <= data_out_prereverse;
            end
        else if ((service_counter == DATA_FRAMES * 2 - 1) || (service_counter == DATA_FRAMES * 2)) begin
            data_out_prereverse[63:52] <= 12'hB40;
            data_out_prereverse[51:26] <= auto_read_i[2];       // 10-bit address, 16-bit data                   
            data_out_prereverse[25:0]  <= auto_read_i[3];       // 10-bit address, 16-bit data
            end
        else if ((service_counter == DATA_FRAMES * 2 + 1) || (service_counter == DATA_FRAMES * 2 + 2)) begin
            data_out_prereverse[63:52] <= 12'hB40;
            data_out_prereverse[51:26] <= auto_read_i[4];      // 10-bit address, 16-bit data
            data_out_prereverse[25:0]  <= auto_read_i[5];      // 10-bit address, 16-bit data
            end
        else if ((service_counter == DATA_FRAMES * 2 + 3)  || (service_counter == DATA_FRAMES * 2 + 4)) begin
            data_out_prereverse[63:52] <= 12'hB40;  // Both regs 'autoread', chip_id and status = '00'
            data_out_prereverse[51:26] <= auto_read_i[6];     // 10-bit address, 16-bit data
            data_out_prereverse[25:0]  <= auto_read_i[7];     // 10-bit address, 16-bit data
        end

        // Reset loop if not due for a CB bundle
        else if ((service_counter >= DATA_FRAMES * 2 + 5) && (service_counter <= DATA_FRAMES * 2 + 11))begin

            if (cb_wait_cnt == CB_WAIT)
                data_out_prereverse <= {8'h78, 4'b0100, 52'b0};     // AURORA code word. Channel Bonding
            else begin // reset loop
                cb_wait_cnt         <= cb_wait_cnt + 1;
                data_out_prereverse <= hitData_muxed;
                service_counter     <= 9'b000000000;
            end

        end

        // Reset loop after a forced CB bundle
        else if((service_counter >= DATA_FRAMES * 2 + 11) && cb_wait_cnt == CB_WAIT) begin
            cb_wait_cnt <= 0;
            data_out_prereverse <= hitData_muxed;
            service_counter <= 7;
        end
        // Data frame (not service frame)
        else if (service_counter[0]) begin
            data_out_prereverse <= hitData_muxed;
        end

        // Data_out_valid logic
        if(((service_counter >= DATA_FRAMES * 2 - 3) && (service_counter <= DATA_FRAMES * 2 + 4)) ||
           ((cb_wait_cnt == CB_WAIT) && (service_counter >= DATA_FRAMES * 2 + 5)))
            data_out_valid_r <= service_counter[0];
        else if(!hitData_empty)
            data_out_valid_r <= service_counter[0];
        else
            data_out_valid_r <= 1'b0;
    end

end


// Drive ports on module
assign data_out = data_out_prereverse;

/*
ilaOut u_ila_CmdOut
    (.clk(clk160)
      ,.probe0(data_out)
      ,.probe1(service_counter)
      ,.probe2(hitData_empty)
      ,.probe3(hitin)
      ,.probe4(service_frame)
      ,.probe5(cmdin)
      ,.probe6(wr_cmd)
      ,.probe7(adxin)
      ,.probe8(wr_adx)
      ,.probe9(data_out_valid)
      ,.probe10(rd_cmd)
      ,.probe11(cmd_out)
      ,.probe12(cmd_full)
      ,.probe13(rd_adx)
      ,.probe14(adx_out)
      ,.probe15(adx_full));
*/

endmodule
