//========================================================================================
//=============================          SCRAMLBER           =============================
//========================================================================================

module scrambler #
(
    parameter TX_DATA_WIDTH = 64
)
(
    input [0:(TX_DATA_WIDTH-1)] data_in,
    output [(TX_DATA_WIDTH+1):0] data_out,
    input enable,
    input [1:0] sync_info,
    input clk,
    input rst
);

    assign data_out = {sync_info, data_in};

endmodule