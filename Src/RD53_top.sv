// RD53_top
// Top level module to the RD53 chip emulator -- contains all internal logic
// testing: emulator_tb.v, emulator.do

module RD53_top 
(
    input              rst              ,
    input              clk20            ,
    input              clk40            ,
    input              clk80            ,
    input              clk160           ,

    input              uart_rxd         ,
    output             uart_txd         ,
    
    input              ttc_data         , 
    input              button1          ,
    input   [3:0]      chip_id          ,
    
    output  [3:0]      cmd_out_p        , 
    output  [3:0]      cmd_out_n        ,

    output             trig_out         ,
    input              ilaActivate      ,
    
    output  [ 2:0]     p_debug           // LEDs debug port -> wrreg,rdreg,trigger_r 
);

logic        word_valid        ;
logic [63:0] frame_out [0:3]   ;
logic [ 0:3] service_frame     ;
logic [ 1:0] sync      [0:3]   ;
logic [ 3:0] data_next         ;


logic [15:0] command            ;
logic [31:0] commandsSent       ;
logic [15:0] data_locked        ;
logic [15:0] data_locked_flipped;



    //------------------------------------------------
    // Parses incoming TTC stream, locks to a channel
    //------------------------------------------------
    ttc_top             u_ttc_top
    (
        .clk160         (clk160         ),
        .rst            (rst            ),

        .datain         (ttc_data       ),

        .valid          (word_valid     ),
        .data           (data_locked    )
    );

    assign command = data_locked;

    assign data_locked_flipped[15] = data_locked[0];
    assign data_locked_flipped[14] = data_locked[1];
    assign data_locked_flipped[13] = data_locked[2];
    assign data_locked_flipped[12] = data_locked[3];
    assign data_locked_flipped[11] = data_locked[4];
    assign data_locked_flipped[10] = data_locked[5];
    assign data_locked_flipped[9]  = data_locked[6];
    assign data_locked_flipped[8]  = data_locked[7];
    assign data_locked_flipped[7]  = data_locked[8];
    assign data_locked_flipped[6]  = data_locked[9];
    assign data_locked_flipped[5]  = data_locked[10];
    assign data_locked_flipped[4]  = data_locked[11];
    assign data_locked_flipped[3]  = data_locked[12];
    assign data_locked_flipped[2]  = data_locked[13];
    assign data_locked_flipped[1]  = data_locked[14];
    assign data_locked_flipped[0]  = data_locked[15];



    //----------------------------------------------
    // Prepare data from RD53 to be sent out
    //----------------------------------------------
    chip_output             u_chip_output
    (
        .rst                (rst            ),
        .clk160             (clk160         ),
        .clk80              (clk80          ),
        .clk40              (clk40          ),
        .clk20              (clk20          ),

        .uart_rxd           (uart_rxd       ),  // input   logic
        .uart_txd           (uart_txd       ),  // output  logic     

        .word_valid         (word_valid     ),
        .data_in            (data_locked    ),
        .chip_id            (chip_id        ),  // Currently not implemented
        .data_next          (|data_next     ),  // All four lines should always present same data
        .button             (ilaActivate    ),
        .button1            (button1        ),

        .frame_out          (frame_out      ),
        .service_frame      (service_frame  ),
        .trig_out           (trig_out       ),
        .p_debug            (p_debug        )
    );


    assign sync[0] = service_frame[0] ? 2'b10 : 2'b01;
    assign sync[1] = service_frame[1] ? 2'b10 : 2'b01;
    assign sync[2] = service_frame[2] ? 2'b10 : 2'b01;
    assign sync[3] = service_frame[3] ? 2'b10 : 2'b01;


    //----------------------------------------------
    // Four lane of Aurora differential output
    //----------------------------------------------
    aurora_tx_four_lane     u_aurora_tx_four_lane
    (
        .rst                (rst            ), // input          
        .clk40              (clk20          ), // input           // TODO ** clk20 connected to clk40 port **
        .clk160             (clk80          ), // input           // TODO ** clk80 connected to clk160 port **

        .data_next          (data_next      ), // output  [3:0]   // Requests more data

        .data_in            (frame_out      ), // input   [63:0] 
        .sync               (sync           ), // input   [ 1:0] 

        .data_out_p         (cmd_out_p      ), // output  [3:0]   // Differential pad output
        .data_out_n         (cmd_out_n      )  // output  [3:0]  
    );


    //----------------------------------------------
    // Debug ILAs
    //----------------------------------------------
    ila_command_received    u_ila_cmd_rcv
    (
       .clk                 (clk160         ),
       .probe0              (rst            ),
       .probe1              (4'h0           ),
       .probe2              (ttc_data       ),
       .probe3              (command        ),
       .probe4              (data_locked    ),
       .probe5              (4'h0           ),
       .probe6              (word_valid     )
    );

    ila_output_data_top_level   u_ila_output_data_top_level
    (
       .clk                 (clk160         ),
       .probe0              (frame_out[0]   ),
       .probe1              (frame_out[1]   ),
       .probe2              (frame_out[2]   ),
       .probe3              (frame_out[3]   ),
       .probe4              (trig_out       ),
       .probe5              (4'h0           ),
       .probe6              (1'b0           )
    ); 

endmodule
