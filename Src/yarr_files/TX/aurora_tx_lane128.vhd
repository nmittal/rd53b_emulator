-------------------------------------------------------------------------------------
-- Company:        LBNL / HEIA-FR
-- Engineer:       Queiroz Maic
-- E-Mail:         mqueiroz at lbl.gov
--                 maic.queiroz at edu.hefr.ch
-- Create Date:    01:24:12 07/03/2018
-- Design Name:
-- Module Name:    aurora_tx_lane128 - Behavioral
-- Project Name:   Pixel data-stream aggregator
-- Target Devices: Xilinx Kintex-7 KC705
-- Tool versions:  Xilinx Vivado v2017.4
-- Description:    Aurora TX Lane, map all the subcomponents
--
-- Additional Comments:  -
--
-------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity aurora_tx_lane128 is
  port (
    rst_i              : in  std_logic;
    clk_i              : in  std_logic;
    clkhigh_i          : in  std_logic;

    -- TX data in
    read_o             : out std_logic;
    data66tx_i         : in  std_logic_vector(65 downto 0);

    dataout_p          : out std_logic;
    dataout_n           : out std_logic
  );
end aurora_tx_lane128;


architecture struct of aurora_tx_lane128 is

----------------------------
-- Components
----------------------------

-- Data scrambler (no impact on the header)
component scrambler     -- Width iparameter set to 64. (Verilog)
port (
    clk             : in  std_logic;
    rst             : in  std_logic;
    enable             : in  std_logic;

    sync_info          : in  std_logic_vector(1 downto 0);
    data_in         : in  std_logic_vector( 0 to 63);

    data_out        : out std_logic_vector(65 downto 0)
);
end component scrambler;

-- Gearbox 66 bit to 32 bit
component gearbox66to32
generic (
    ratio_g          : integer := 4
);
port (
      rst_i            : in  std_logic;
      clk_i            : in  std_logic;

      data66_i         : in  std_logic_vector(65 downto 0);

      data32_o         : out std_logic_vector(31 downto 0);
      data32_valid_o   : out std_logic;
      read_o           : out std_logic
);
end component;
  
-- 32bit to 8 bit (required to have the right serdes input width)
component serdes32to8
port (
      rst_i            : in  std_logic;
      clk_i            : in  std_logic;

      data32_i         : in  std_logic_vector(31 downto 0);
      data32_valid_i   : in  std_logic;

    data8_o          : out std_logic_vector(7 downto 0)
);
end component;

-- Serdes 8 to 1
component serdes8to1
port (
      rst_i            : in  std_logic;
      clk_i            : in  std_logic;

      clkhigh_i        : in  std_logic;
      data8_i          : in  std_logic_vector(7 downto 0);

      TQ_o             : out std_logic;
      dataout_p        : out std_logic;
    dataout_n       : out std_logic
);
end component;

  signal data66_s        : std_logic_vector(65 downto 0);     -- 66 bit Header + Block
  signal data32_s        : std_logic_vector(31 downto 0);     -- 32-bit chunks
  signal data32_valid_s  : std_logic;                         -- 32-bit chunks validity
  signal data8_s         : std_logic_vector(7 downto 0);      -- 8-bit chunks
  signal readandscramb_s : std_logic;                         -- Read block and scramble flag

-- Constants
constant RATIO          : integer := 4;      -- TODO Add meaningfull comment

begin

    --  Generics checking
    assert (RATIO >= 4)
    report "aurora_tx_lane128, generic constant RATIO error: ratio must be 4 minimum"
    severity failure;


    --------------------------------------------------------
    --
    --------------------------------------------------------
    u_scrambler : scrambler 
    port map (
        clk             => clk_i,                       -- input                        
        rst             => rst_i,                       -- input                        
  
        enable          => readandscramb_s,             -- input   from gearbox
        sync_info       => data66tx_i(65 downto 64),    -- input   [1:0]                
        data_in         => data66tx_i(63 downto 0),     -- input   [0:(TX_DATA_WIDTH-1)]

        data_out        => data66_s                     -- output  [(TX_DATA_WIDTH+1):0]
    );


    --------------------------------------------------------
    --
    --------------------------------------------------------
    u_gearbox66to32 : gearbox66to32
    generic map (
        ratio_g => RATIO
    )
    port map(
        rst_i           => rst_i,
        clk_i           => clk_i,

        data66_i        => data66_s,

        data32_o        => data32_s,
        data32_valid_o  => data32_valid_s,

        read_o          => readandscramb_s  -- to 'read_o'
    );
      
    --------------------------------------------------------
    --
    --------------------------------------------------------
    u_serdes32to8 : serdes32to8
    port map(
        rst_i           => rst_i,
        clk_i           => clk_i,

        data32_i        => data32_s,
        data32_valid_i  => data32_valid_s,

        data8_o         => data8_s
    );
      
    --------------------------------------------------------
    -- 8-to-1 serdes receiving data8_s from 32-to-8 serdes
    --------------------------------------------------------
    u_serdes1to8 : serdes8to1
    port map(
        rst_i           => rst_i,
        clk_i           => clk_i,
        clkhigh_i       => clkhigh_i,

        data8_i         => data8_s,

        TQ_o            => open,

        dataout_p       => dataout_p,
        dataout_n       => dataout_n
    );

    read_o  <= readandscramb_s;     -- From gearbox. Used to request more data from upstream blocks
  
end struct;

