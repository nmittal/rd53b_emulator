//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
// When the input next_hit is raised, the next trigger value is processed
// and if the next trigger value is high, new data will be generated.
//----------------------------------------------------------------------------

`timescale 1ps/1ps
module hitMaker3
(
    input           rst                 , 
    input           tClk                ,   //80MHz Clock to write to trigger FIFO and trigger_tag FIFO
    input           hClk                ,   //40MHz
    input           dClk                ,   //160MHz

    input           uart_rxd            ,
    output          uart_txd            ,

    input           emptyTT             ,
    input           writeT              , 
    input           calSig              ,   // Unused
    input   logic   [15:0] hitDataConfig,
    input   [ 3:0]  triggerClump        , 
    input   [ 7:0]  triggerTag          ,
    input   [31:0]  seed1               ,   // Unused
    input   [31:0]  seed2               ,   // Unused
    input           next_hit            ,   // Read one word from hit FIFO
    input           update_output_i     ,
    output  [63:0]  hitData             ,
    output          full                ,
    output          empty               ,
    output          hitData_empty       ,
    output          p_debug               //LED debug port
);

//------------------------------------------------------------------------

    reg             doAStep;
    reg             sawAnEmpty;
    wire            step;
    wire    [63:0]  maskedData;
    
    wire            holdDataFull;
    wire            doWriteT;
    reg             doRead;   
    reg             done;   
    logic           trigger;
    logic           trigger_r;
    logic   [ 1:0]  bit_order_tag;
    logic   [63:0]  hit_gen_data_tagged;          //correct tag + stream
    
    assign doWriteT = writeT & (~full);
    
    //------------------------------------------------------------------------
    // Reverse the order of the trigger so the fifo extracts it in the correct order
    //------------------------------------------------------------------------
    wire    [3:0]   iTriggerClump;
    assign iTriggerClump[0] = triggerClump[3];
    assign iTriggerClump[1] = triggerClump[2];
    assign iTriggerClump[2] = triggerClump[1];
    assign iTriggerClump[3] = triggerClump[0];
    
    
    logic first_rd_en;

    //------------------------------------------------------------------------
    // FIFO for triggers 32x4 -> 128x1   (Actual 31x4 -> 124x1
    //  Write 4-bit trigger patterns. Read 1-bit triggers 
    //------------------------------------------------------------------------
    triggerFifo u_triggerFifo 
    (
        .rst        ( emptyTT       ), 
        .wr_clk     ( tClk          ),
        .rd_clk     ( hClk          ), 
        .din        ( iTriggerClump ), 
        .wr_en      ( doWriteT      ), 
        .rd_en      ( first_rd_en   ), 
        .dout       ( step          ),
        .full       ( full          ), 
        .empty      ( empty         )
    );

    
    //------------------------------------------------------------------------
    // Decide when to read new values
    //------------------------------------------------------------------------
    logic [1:0] wait_period;
    logic valid_step;

    always @(posedge hClk) 
    begin
        if (rst) begin
            first_rd_en <= 0;
            wait_period <= 0;
            valid_step  <= 0;

        // If empty or not done don't pull new values
        end else if (empty | ~done) begin
            first_rd_en <= 0;
            wait_period <= 0;
            valid_step  <= 0;

       // Get a new value out
        end else if (~empty & wait_period == 0) begin
            first_rd_en <= 1;
            wait_period <= 1;
            valid_step  <= 0;
        end else if (wait_period == 1) begin
            first_rd_en <= 0;
            wait_period <= 2;
            valid_step  <= 1;
        end else if (wait_period == 2) begin
            first_rd_en <= 0;
            wait_period <= 3;
            valid_step  <= 0;
        end else if (~empty & wait_period == 3) begin

            // if 0 keep pulling
            if (step == 0) begin
                first_rd_en <= 1;
                wait_period <= 1;
                valid_step  <= 0;

            // if 1 cause a cycle delay
            end else begin
                first_rd_en <= 0;
                wait_period <= 0;
                valid_step  <= 0;
            end

        end else begin
            first_rd_en <= 0;
            wait_period <= 0;
            valid_step  <= 0;
        end
    end

    wire [5:0] tagInfoIn;
    wire [5:0] tagInfoOut;
    reg [2:0] tagCounter;
    assign tagInfoIn[5:0] = triggerTag[5:0];
    assign bit_order_tag  = tagCounter - 1;
    
    logic full1, empty1;

    //------------------------------------------------------------------------
    // FIFO for trigger tags
    //------------------------------------------------------------------------
    triggerTagFifo u_triggerTagFifo 
    (
        .rst    		( emptyTT       ), 
        .wr_clk 		( tClk          ),
        .rd_clk 		( hClk          ), 
        .din    		( tagInfoIn     ), 
        .wr_en  		( doWriteT      ), 
        .rd_en  		( doRead        ), 
        .dout   		( tagInfoOut    ),
        .full   		( full1         ), 
        .empty  		( empty1        ),
		.wr_rst_busy 	(				),
        .rd_rst_busy 	(				)
    );


    reg [4:0] tagID;
    reg [14:0] BCID;

    //------------------------------------------------------------------------
    // Counter that manages when tag info should be read from second fifo
    //------------------------------------------------------------------------
   
    always @(posedge hClk)
    begin
        if (rst) begin
            tagCounter  <= 0;
            doRead      <= 0;
        end else if (empty) begin
            tagCounter  <= 0;
            doRead      <= 0;
        end else if (tagCounter == 4 & first_rd_en) begin
            tagCounter  <= 1;
            doRead      <= 1;
        end else if (tagCounter == 0 & first_rd_en) begin
            tagCounter  <= tagCounter + 1;
            doRead      <= 1;
        end else if (first_rd_en) begin
            tagCounter  <= tagCounter + 1;
            doRead      <= 0;
        end else begin
            tagCounter  <= tagCounter;  // Useless line
            doRead      <= 0;
        end
    end
    
    //------------------------------------------------------------------------
    // Manage internal tag IDs
    //------------------------------------------------------------------------
    always @(posedge hClk) begin
        if (rst) begin
            tagID   <= 31;
        end else if (valid_step & step) begin
            tagID   <= tagID + 1;
        end else begin
            tagID   <= tagID;   // Useless line
        end
    end
    
    //------------------------------------------------------------------------
    // Increment Bunch Crossing ID (BCID) counter every clock cycle
    //------------------------------------------------------------------------
    always @(posedge hClk) 
    begin
        if (rst) begin
            BCID <= 0;
        end else begin
            BCID <= BCID + 1;
        end
    end
    
    //------------------------------------------------------------------------
    // Delay some signals by one clock cycle (Why?)
    //------------------------------------------------------------------------
    reg [ 4:0]  tagID_r;
    reg [ 4:0]  tagInfoOut_r;
    reg [14:0]  BCID_r;

   /* always @(posedge hClk) 
    begin
        if (rst) begin
            tagID_r         <= 0;
            tagInfoOut_r    <= 0;
            BCID_r          <= 0;
        end else begin
            tagID_r         <= tagID;
            tagInfoOut_r    <= tagInfoOut[4:0];
            BCID_r          <= BCID;
        end
    end */
    
    logic [31:0] trigger_info_i;

    assign trigger_info_i[31:8]  = 24'h0;
    assign trigger_info_i[7:0]   = {tagInfoOut, bit_order_tag};
    
    logic empty_r;

    
    //------------------------------------------------------------------------
    //------------------------------------------------------------------------
    always @(posedge hClk) begin
        if (rst)
            empty_r     <= 0;
        else
            empty_r     <= empty;
    end
    

    assign trigger = step & (~empty_r) & valid_step;
    
    //------------------------------------------------------------------------
    //------------------------------------------------------------------------
    always @(posedge hClk) 
    begin
        if (rst)
            trigger_r   <= 0;
        else
            trigger_r   <= trigger;
    end
    
    //------------------------------------------------------------------------
    // Output stream data from a memory table when triggered 
    //------------------------------------------------------------------------
    //hit_generator4  u_hit_generator4      // This hit_generator uses a CPU to load a dual port RAM with data
    hit_generator3  u_hit_generator3        // This hit_generator uses a ROM pre-loaded with hit data from a COE file
    (
        .rst_i          ( rst               ),
        .clk_i          ( hClk              ), 

        .uart_rxd       ( uart_rxd          ),  // input   logic
        .uart_txd       ( uart_txd          ),  // output  logic     

        .config_reg_i   ( hitDataConfig     ),  // input [15:0] (Unused)
        .trigger_i      ( trigger_r         ),  // input
        .trigger_info_i ( trigger_info_i    ),  // input [31:0] (Unused) 

        .trigger_data_o ( maskedData        ),  // output [63:0] Output data stream
        .done_o         ( done              )   // output        Low when data is being output
    );
    
    //debug port connecting to one of the LEDs
    assign p_debug = trigger_r;

    always_comb 
       begin
         if(~done && maskedData[63])
           begin
             hit_gen_data_tagged = {maskedData[63],trigger_info_i[7:0],maskedData[54:0]};
           end
         else
           begin
             hit_gen_data_tagged = maskedData;
           end
       end
    //------------------------------------------------------------------------
    // FIFO to store output data from the hitGenerator block
    //------------------------------------------------------------------------
    hitDataFIFO     u_hitDataFIFO 
    (
        .rst            ( rst               ),
        .wr_clk         ( hClk              ),
        .wr_en          ( ~done             ),
        .din            ( hit_gen_data_tagged       ),

        .rd_clk         ( dClk              ),
        .rd_en          ( next_hit          ),
        .dout           ( hitData           ),
        .full           ( holdDataFull      ),
        .empty          ( hitData_empty     )
    );
    
    ila_hitgen_fifo ila_6
    (
        .clk   (dClk               ),
        .probe0(hit_gen_data_tagged),
        .probe1(hitData            ),
        .probe2(next_hit           ),
        .probe3(hitData_empty      )
    );
    
endmodule
